import "./App.css";
import Headers from "./components/Headers";
import Dashboard from "./components/Dashboard";
import useMediaQuery from "@material-ui/core/useMediaQuery";
import Admin from "./components/Admin";
import Help from "./components/Help";
import Profile from "./components/Profile";
import { BrowserRouter as Router, Switch, Route } from "react-router-dom";

function App() {
  const isSmall = useMediaQuery("(min-width:768px)");
  return (
    <Router>
      <div className="App">
        <Headers isSmall={isSmall} />
        <Switch>
          <Route path="/" exact component={Dashboard} />
          <Route path="/admin" component={Admin} />
          <Route path="/help" component={Help} />
          <Route path="/profile" component={Profile} />
        </Switch>
      </div>
    </Router>
  );
}

export default App;
