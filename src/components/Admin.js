import React, {useEffect} from "react";
import { useState } from "react";
import { makeStyles } from '@material-ui/core/styles';
import Card from '@material-ui/core/Card';
import CardActionArea from '@material-ui/core/CardActionArea';
import CardActions from '@material-ui/core/CardActions';
import CardContent from '@material-ui/core/CardContent';
import CardMedia from '@material-ui/core/CardMedia';
import Button from '@material-ui/core/Button';
import Typography from '@material-ui/core/Typography';
import Grid from '@material-ui/core/Grid'

const useStyles = makeStyles({
  root: {
    maxWidth: 345,
  },
  media: {
    height: 140,
  },
});

function Admin() {
    const [listA, setListA] = useState([]);
    useEffect(() => {
        fetchDetails();
    }, []);

    const fetchDetails = async () => {
        const bannersData = await fetch('https://fortnite-api.com/v1/banners');
        const items = await bannersData.json();
        setListA(items.data.splice(items.data.length-12, 12));
    }
    const classes = useStyles();
  return (
    <>
      <h1>Admin</h1>
     
      <Grid container spacing={3}>
      {listA.map((item, index) => (
          <Grid item xs={4}>
      <Card key={item.id} className={classes.root}>
      <CardActionArea>
        <CardMedia
          className={classes.media}
          image={item.images.icon}
          title={item.devName}
        />
        <CardContent>
          <Typography gutterBottom variant="h5" component="h2">
            {item.name} {index+1}
          </Typography>
          <Typography variant="body2" color="textSecondary" component="p">
            {item.description}
          </Typography>
        </CardContent>
      </CardActionArea>
      <CardActions>
        <Button size="small" color="primary">
          {item.category}
        </Button>
      </CardActions>
    </Card></Grid>
    ))}
    </Grid>
    
    </>
  );
}

export default Admin;
