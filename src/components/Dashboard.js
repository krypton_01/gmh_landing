import React from "react";
import Grid from "@material-ui/core/Grid";
import GridList from "@material-ui/core/GridList";
import GridListTile from "@material-ui/core/GridListTile";
import Img1 from "../assets/images/file_transfer.png";
import Img2 from "../assets/images/data-api.png";
import Img3 from "../assets/images/trading_api_bridges.png";
import Img4 from "../assets/images/data_ops.png";
import Img5 from "../assets/images/reporting.png";
import dbImg from "../assets/images/solution_evolution.png";
import useMediaQuery from "@material-ui/core/useMediaQuery";

function Dashboard() {
  const textVal = "GMH Bulletin Board (Message Board)";
  const isSmall = useMediaQuery("(min-width:768px)");
  const imgStyle = {
    width: "90%",
    height: "90px",
  };

  const imgList = [Img1, Img2, Img3, Img4, Img5];
  return (
    <>
      {!isSmall ? (
        <span>
          <GridList cols={1}>
            <GridListTile style={{ height: "550px" }}>
              <img
                src={dbImg}
                style={{ width: "80%", height: "500px" }}
                alt="Dashboard"
              />
            </GridListTile>
          </GridList>
          <GridList cols={1}>
            <GridListTile style={{ height: "550px" }}>
              <Grid
                container
                direction="column"
                justify="center"
                alignItems="center"
              >
                {imgList.map((tile) => (
                  <Grid key={tile} item xs={12}>
                    <img style={imgStyle} src={tile} alt={tile} />
                  </Grid>
                ))}
              </Grid>
            </GridListTile>
          </GridList>
        </span>
      ) : (
        <GridList cols={2}>
          <GridListTile style={{ height: "550px", width: "75%" }}>
            <img
              src={dbImg}
              style={{ width: "80%", height: "500px" }}
              alt="Dashboard"
            />
          </GridListTile>

          <GridListTile
            style={{ height: "550px", width: "25%", marginTop: "20px" }}
          >
            <Grid
              container
              direction="column"
              justify="flex-start"
              alignItems="flex-start"
            >
              {imgList.map((tile) => (
                <Grid key={tile} item xs={12}>
                  <img style={imgStyle} src={tile} alt={tile} />
                </Grid>
              ))}
            </Grid>
          </GridListTile>
        </GridList>
      )}
      <textarea rows="8" style={{ width: "95%" }} value={textVal} disabled />
    </>
  );
}

export default Dashboard;
