import React, { useState } from "react";
import { makeStyles } from "@material-ui/core/styles";
import AppBar from "@material-ui/core/AppBar";
import Toolbar from "@material-ui/core/Toolbar";
import Typography from "@material-ui/core/Typography";
import Button from "@material-ui/core/Button";
import IconButton from "@material-ui/core/IconButton";
import MenuIcon from "@material-ui/icons/Menu";
import Drawer from "@material-ui/core/Drawer";
import MenuItem from "@material-ui/core/MenuItem";
import { Link } from "react-router-dom";
import "./Drawer.css";

const useStyles = makeStyles((theme) => ({
  root: {
    flexGrow: 1,
  },
  menuButton: {
    marginRight: theme.spacing(2),
  },
  title: {
    flexGrow: 1,
  },
}));
function Headers({ isSmall }) {
  const [state, setState] = useState({
    drawerOpen: false,
  });
  const { drawerOpen } = state;
  const handleDrawerOpen = () => {
    setState((prevState) => ({ ...prevState, drawerOpen: true }));
  };

  const handleDrawerClose = () => {
    setState((prevState) => ({ ...prevState, drawerOpen: false }));
  };

  const headerList = [
    "Admin",
    "Refresh",
    "Help",
    "Profile",
    { data: "Admin", route: "/admin" },
    { data: "Refresh", route: "/" },
    { data: "Help", route: "/help" },
    { data: "Profile", route: "/profile" },
  ];
  const classes = useStyles();
  const linkStyle = {
    color: "white",
    textDecoration: "none",
  };
  return (
    <>
      {!isSmall ? (
        <AppBar position="static">
          <Toolbar>
            <IconButton
              edge="start"
              className={classes.menuButton}
              color="inherit"
              aria-label="menu"
              onClick={handleDrawerOpen}
            >
              <MenuIcon />
            </IconButton>

            <Drawer
              anchor="left"
              open={drawerOpen}
              onClose={handleDrawerClose}
              className="drawerStyle"
            >
              <div style={{ padding: "20px 30px" }}>
                {headerList.map((item, index) => (
                  <Link to={item.route} style={linkStyle} key={index}>
                    <MenuItem onClick={handleDrawerClose}>
                      {item.data}
                    </MenuItem>
                  </Link>
                ))}
              </div>
            </Drawer>

            <Typography variant="h6" className={classes.title}>
              <Link to="/" style={linkStyle}>
              GMH Dashboard
              </Link>
            </Typography>
          </Toolbar>
        </AppBar>
      ) : (
        <AppBar position="static">
          <Toolbar>
            <Typography variant="h6" className={classes.title}>
            <Link to="/" style={linkStyle}>
              GMH Dashboard
              </Link>
            </Typography>
            {headerList.map((item, index) => (
              <Link to={item.route} style={linkStyle} key={index}>
                <Button color="inherit">
                  {item.data}
                </Button>
              </Link>
            ))}
          </Toolbar>
        </AppBar>
      )}
    </>
  );
}

export default Headers;
